import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GaureshComponent } from './gauresh/gauresh.component';
import { VvComponent } from './vv/vv.component';
import { Gm1Component } from './gm1/gm1.component';
import { Vv1Component } from './vv1/vv1.component';
import { ChooseComponent } from './choose/choose.component';

@NgModule({
  declarations: [
    AppComponent,
    GaureshComponent,
    VvComponent,
    Gm1Component,
    Vv1Component,
    ChooseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
