import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gm1Component } from './gm1.component';

describe('Gm1Component', () => {
  let component: Gm1Component;
  let fixture: ComponentFixture<Gm1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gm1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gm1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
