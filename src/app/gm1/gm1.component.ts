import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-gm1',
  templateUrl: './gm1.component.html',
  styleUrls: ['./gm1.component.scss']
})
export class Gm1Component implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  next(){
    this.router.navigate(['/gaureshmeet']);
  }

}
