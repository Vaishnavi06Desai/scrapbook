import { Component, OnInit } from '@angular/core';
import { Gm1Component } from '../gm1/gm1.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-choose',
  templateUrl: './choose.component.html',
  styleUrls: ['./choose.component.scss']
})
export class ChooseComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  gm(){
    this.router.navigate(['/rakhi1']);
  }

  vv(){
    this.router.navigate(['/rakhi2']);
  }

}
