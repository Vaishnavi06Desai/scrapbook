import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Vv1Component } from './vv1.component';

describe('Vv1Component', () => {
  let component: Vv1Component;
  let fixture: ComponentFixture<Vv1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Vv1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Vv1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
