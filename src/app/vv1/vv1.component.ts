import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vv1',
  templateUrl: './vv1.component.html',
  styleUrls: ['./vv1.component.scss']
})
export class Vv1Component implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  next(){
    this.router.navigate(['/virajvrishabh']);
  }

}
