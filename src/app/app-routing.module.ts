import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GaureshComponent } from './gauresh/gauresh.component';
import { VvComponent } from './vv/vv.component';
import { Gm1Component } from './gm1/gm1.component';
import { Vv1Component } from './vv1/vv1.component';
import { ChooseComponent } from './choose/choose.component';


const routes: Routes = [
  {
    path :'gaureshmeet',
    component: GaureshComponent
  },
  {
    path :'choose',
    component: ChooseComponent
  },
  {
    path :'virajvrishabh',
    component: VvComponent
  },
  {
    path :'rakhi1',
    component: Gm1Component
  },
  {
    path :'rakhi2',
    component: Vv1Component
  },
  {
    path :'',
    redirectTo: 'choose',
    pathMatch: 'full'
  },
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
