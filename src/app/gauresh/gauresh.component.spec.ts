import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GaureshComponent } from './gauresh.component';

describe('GaureshComponent', () => {
  let component: GaureshComponent;
  let fixture: ComponentFixture<GaureshComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GaureshComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GaureshComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
